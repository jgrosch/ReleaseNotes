#!/bin/bash

set -ex

ID=`id -u`
if [ $ID != 0 ]
then
    echo "ERROR: Only root can run this."
    exit 1
fi

HERE=`pwd`

USER="$1"
GROUP="$2"
BASE="$3"

if [ -z "$USER" ] || [ -z "$GROUP" ]; then
  echo "User and group must be provided"
  exit 1
fi

if [ ! -e $BASE ]
then
    mkdir -p $BASE/{data,etc,templates}
    mkdir -p $BASE/data/ReleaseHighlights/{CentOS,Common,FreeBSD,RedHat,Ubuntu}
    chmod 0755 $BASE
    chown $USER:$GROUP $BASE
    cd $BASE
    for f in `find -type d`
    do
        chmod 0755 $f
        chown $USER:$GROUP $f
    done
fi

cd $HERE/etc
cp *.json $BASE/etc

cd $HERE/data/ReleaseHighlights/Common
cp *.html $BASE/data/ReleaseHighlights/Common

cd $HERE/templates
cp *.j2 $BASE/templates

cd $BASE
for f in `find -type f`
do
    chmod 0644 $f
    chown $USER:$GROUP $f
done

cd $HERE
cp *.py $BASE
chmod 0755 $BASE/*.py
chown $USER:$GROUP $BASE/*.py

ln -sf $BASE/genJuniperFacts.py /usr/local/bin
ln -sf $BASE/genReleaseNotes.py /usr/local/bin
