#!/usr/bin/env python3

import os, sys
import json

def loadOsValues(D):
    osValues = {}
    osInfoFile = 'osInfo.json'
    fp = open(osInfoFile, 'r')
    Lines = fp.read()
    fp.close()
    
    data = json.loads(Lines)
    Values = data['osValues']

    for entry in Values:
        osDir          = entry['osDir']
        osName         = entry['osName']
        osRel          = entry['osRel']
        packerSource   = entry['packerSource']
        templateString = entry['templateString']

        key   = osRel

        value = "{\"osDir\": \"%s\", \"osName\": \"%s\", \"osRel\": \"%s\", \"packerSource\": \"%s\", \"templateString\": \"%s\"}" % (osDir, osName, osRel, packerSource, templateString)

        osValues[key] = value

    D['osValues'] = osValues
    
    return
    #
    # End of loadOsValues
    #
    
def main():

    D = {}

    osQuery = 'C7'
    
    loadOsValues(D)
    osValues = D['osValues']
    
    osInfo = eval(osValues[osQuery])
    
    osDir          = osInfo['osDir']
    osName         = osInfo['osName']
    osRel          = osInfo['osRel']
    packerSource   = osInfo['packerSource']
    templateString = osInfo['templateString']

    
    sys.exit(0)
    #
    # End of main
    #

if __name__ == '__main__':
    main()
