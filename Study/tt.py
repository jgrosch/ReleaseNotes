#!/usr/bin/env python3

import os, sys

def main():
    j = 0
    
    osValues = {
        'C5' : {
            'osRel'  : 'C5',
            'osDir'  : 'CentOS',
            'osName' : 'CentOS 5',
            'templateString' : 'c5-pt',
            'packerSource' : 'c5-ps'
            },
        'C6' : {
            'osRel'  : 'C6',
            'osDir'  : 'CentOS',
            'osName' : 'CentOS 6',
            'templateString' : 'c6-pt',
            'packerSource' : 'c6-ps'
            },
        'C7' : {
            'osRel'  : 'C7',
            'osDir'  : 'CentOS',
            'osName' : 'CentOS 7',
            'templateString' : 'c7-pt',
            'packerSource' : 'c7-ps'
            },
        'R5' : {
            'osRel'  : 'R5',
            'osDir'  : 'RedHat',
            'osName' : 'RedHat 5',
            'templateString' : 'r5-pt',
            'packerSource' : 'r5-ps'
            },
        'R6' : {
            'osRel'  : 'R6',
            'osDir'  : 'RedHat',
            'osName' : 'RedHat 6',
            'templateString' : 'r6-pt',
            'packerSource' : 'r6-ps'
            },
        'R7' : {
            'osRel'  : 'R7',
            'osDir'  : 'RedHat',
            'osName' : 'RedHat 7',
            'templateString' : 'r7-pt',
            'packerSource' : 'r7-ps'
            },
        'F10' : {
            'osRel'  : 'F10',
            'osDir'  : 'FreeBSD',
            'osName' : 'FreeBSD 10',
            'templateString' : 'f10-pt',
            'packerSource' : 'f10-ps'
            },
        'F11' : {
            'osRel'  : 'F11',
            'osDir'  : 'FreeBSD',
            'osName' : 'FreeBSD 11',
            'templateString' : 'f11-pt',
            'packerSource' : 'f11-ps'
            },
        'U12' : {
            'osRel'  : 'U12',
            'osDir'  : 'Ubuntu',
            'osName' : 'Ubuntu 12',
            'templateString' : 'u12-pt',
            'packerSource' : 'u12-ps'
            },
        'U14' : {
            'osRel'  : 'U14',
            'osDir'  : 'Ubuntu',
            'osName' : 'Ubuntu 14',
            'templateString' : 'u14-pt',
            'packerSource' : 'u14-ps'
            },
        'U16' : {
            'osRel'  : 'U16',
            'osDir'  : 'Ubuntu',
            'osName' : 'Ubuntu 16',
            'templateString' : 'u16-pt',
            'packerSource' : 'u16-ps'
            },
        'U18' : {
            'osRel'  : 'U18',
            'osDir'  : 'Ubuntu',
            'osName' : 'Ubuntu 18',
            'templateString' : 'u18-pt',
            'packerSource' : 'u18-ps'
            }
        }

    i = 0

    osInfo = osValues['C7']
    packerSource = osInfo['packerSource']
    
    
    sys.exit(0)
    #
    # End of main
    #

if __name__ == '__main__':
    main()
