#!/bin/bash

set -x 

OPTIONS="-av --delete "
HOST="p-jps-osr-bast.juniper.net"
TIME=`date`

echo "// -----------------------------------------------------"
echo "//"
echo "// Pushing Release Notes"
echo "//"
echo "// Date: $TIME"
echo "//"
echo "// -----------------------------------------------------"

cd ../WWW
rsync $OPTIONS -e "-i $ITA_KEY " * itadmin@$HOST:/u3/Other/html/ReleaseNotes

echo "// -----------------------------------------------------"
echo "//"
echo "// Done pushing Release Notes"
echo "//"
echo "// Date: $TIME"
echo "//"
echo "// -----------------------------------------------------"

exit 0
