#!/usr/bin/env python3

import os, sys
import pprint

def main():
    baseDir = '/u3/Other/html/ReleaseNotes'
    OS = ['CentOS', 'FreeBSD', 'RedHat', 'Ubuntu']

    # R2[qIndex] = {'fileName': fqFile, 'date': fileDate} 
    R        = {}
    R2       = {}
    
    Quarters = {}
    quarter  = 0
    qIndex   = ""
    
    os.chdir(baseDir)

    for root, dirs, files in os.walk(baseDir, topdown=False):
        for fileName in files:
            fqFile = os.path.join(root, fileName)
            print(fqFile)
            R[fileName] = fqFile

    Keys = R.keys()
    for key in Keys:
        bits = key.split('-')
        if bits[0] == 'JNPR':
            year  = bits[2]
            month = bits[3]
            day   = bits[4]

            keyName = key
            monthInt = int(month)
            
            fileDate = "{}-{}-{}".format(year, month, day)

            if monthInt >= 1 and monthInt <= 3:
                quarter = 1
                qIndex = "quarter1"
                
            elif monthInt >= 4 and monthInt <= 6:
                quarter = 2
                qIndex = "quarter2"

            elif monthInt >= 7 and monthInt <= 9:
                quarter = 3
                qIndex = "quarter3"

            elif monthInt >= 10 and monthInt <= 12:
                quarter = 4
                qIndex = "quarter4"

            R2[keyName] = {'fileName': fqFile, 'date': fileDate, 'quarter': qIndex} 
                
        elif bits[0] == 'RN':
            continue

    pprint.pprint(R2)
    
    i = 0

    sys.exit(0)
    #
    # End of main
    #

if __name__ == '__main__':
    main()
    
