#!/bin/base -e

set -x 

GIT_BASE="Git/ReleaseNotes"

usage() {
  echo "Usage:"
  echo "genReleaseNotes.sh <IP Address>"
  echo ""
  echo "Valid image types: base, ci, prod"
}

if [ $# -lt 1 ]; then
   usage
   exit 1
fi

HOST="${1}"

cd $GIT_BASE 
git pull
$GIT_BASE/genReleaseNotes.py --common --place --host ${HOST}
$GIT_BASE/Tools/pushReleaseNotes.sh


exit 0


