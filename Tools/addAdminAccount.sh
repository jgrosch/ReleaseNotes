#!/bin/bash

# -------------------------------------------------------------------
#
#                         <addAdminAccount.sh >
#
# -------------------------------------------------------------------


# -------------------------------------------------------------------
#
# File Name    : AddAdminAccount
#
# Author       : Josef Grosch
#                         
# Date         : 20 June 2020
#
# Version      : 1.0
#
# Modification : 
#
# Application  :
#
# Functions    :
#
# Description  :
#
# Notes        : 
#
# -------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                              Copyright
#
#                   (C) Copyright 2020 Moose River LLC.
#
#                         All Rights Reserved
#
#                 Deadicated to my brother Jerry Garcia,
#              who passed from this life on August 9, 1995.
#                       Happy trails to you, Jerry
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                                GPG Key
#
# pub rsa4096 2020-04-30 [SC] [expires: 2022-04-30]
# Key fingerprint = B696 2527 855D 2098 CAD3  E240 F699 5A87 E863 BA0F
# uid [ultimate] Josef Grosch <jgrosch@gmail.com>
# sub rsa4096 2020-04-30 [E] [expires: 2022-04-30]
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#
#                          Contact Information
#
#                           Moose River LLC.
#                             P.O. Box 9403
#                          Berkeley, Ca. 94709
#
#                       http://www.mooseriver.com
#
# -----------------------------------------------------------------------

set -x

ADMIN_GROUP="msadmingroup"
ADMIN_USER="msadmin"

ID=`id -u`
if [ $ID != 0 ];
then
    echo "# --------------------------------------------------------------------"
    echo "# "
    echo "# Only root can run this script."
    echo "# "
    echo "# --------------------------------------------------------------------"
    exit 1
else
    echo "# --------------------------------------------------------------------"
    echo "# "
    echo "# You are root"
    echo "# "
    echo "# --------------------------------------------------------------------"
fi


# -------------------------------------------------------------------
#
# What type OS is this
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# What OS is this."
echo "# "
echo "# --------------------------------------------------------------------"

OS_TYPE="NONE"

OS=`uname -s`
if [ $OS == "FreeBSD" ];
then
    OS_TYPE="FreeBSD"
    setenv DEFAULT_ALWAYS_YES yes
    setenv ASSUME_ALWAYS_YES yes
elif [ $OS == "Linux" ];
then
    if [ -e /etc/debian_version ];
    then
        OS_TYPE="DEBIAN"
    elif [ -e /etc/redhat-release ];
    then 
        OS_TYPE="REDHAT"
    fi   
fi

if [ $OS_TYPE == "NONE" ];
then
    echo "# --------------------------------------------------------------------"
    echo "# "
    echo "# ERROR: I'm so confused."
    echo "# I don't know what sort of OS this is."
    echo "# "
    echo "# --------------------------------------------------------------------"
    exit 1
else
    echo "# --------------------------------------------------------------------"
    echo "# "
    echo "# This is a $OS_TYPE system."
    echo "# "
    echo "# --------------------------------------------------------------------"
fi


# -------------------------------------------------------------------
#
# add group $ADMIN_GROUP
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Adding the admin group $ADMIN_GROUP."
echo "# "
echo "# --------------------------------------------------------------------"
if [ $OS_TYPE == "FreeBSD" ];
then
    /usr/sbin/pw group add $ADMIN_GROUP
elif [ $OS_TYPE == "DEBIAN" ];
then
    /usr/sbin/addgroup $ADMIN_GROUP
elif [ $OS_TYPE == "REDHAT" ];
then
    /usr/sbin/groupadd $ADMIN_GROUP
fi


# -------------------------------------------------------------------
#
# add user $ADMIN_USER
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Adding the admin user $ADMIN_USER."
echo "# "
echo "# --------------------------------------------------------------------"
if [ $OS_TYPE == "FreeBSD" ];
then
    pkg info | grep bash-static
    RC=$?
    if [ $RC == 1 ];
    then
        pkg install bash-static
        cp /usr/local/bin/bash /bin
        echo "/bin/bash" >> /etc/shells
    fi    
    /usr/sbin/pw useradd $ADMIN_USER -m -G $ADMIN_GROUP -s /bin/bash  
else
    /usr/sbin/useradd -m -G $ADMIN_GROUP -s /bin/bash $ADMIN_USER
fi


# -------------------------------------------------------------------
#
# change $ADMIN_USER passwd
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Changing the password for $ADMIN_USER."
echo "# "
echo "# --------------------------------------------------------------------"
if [ $OS_TYPE == "FreeBSD" ];
then
    echo 'ChangeMe!' | /usr/sbin/pw usermod $ADMIN_USER -h 0
else
    echo "$ADMIN_USER:ChangeMe!" | /usr/sbin/chpasswd
fi


# -------------------------------------------------------------------
#
# create .ssh
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Creating the .ssh directory for $ADMIN_USER."
echo "# "
echo "# --------------------------------------------------------------------"
cd /home/$ADMIN_USER
mkdir .ssh
chmod 0700 .ssh
chown -R $ADMIN_USER:$ADMIN_USER /home/$ADMIN_USER


# -------------------------------------------------------------------
#
# add key
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Adding the ssh key for $ADMIN_USER."
echo "# "
echo "# --------------------------------------------------------------------"
cd /home/$ADMIN_USER/.ssh
OUT="/home/$ADMIN_USER/.ssh/authorized_keys"

cat <<EOF >$OUT
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVBATDjYszwyhXmy/jBgwHrT3iiRR03RNaTsrIC0wf0gWc71EVr4DmsLFUAEhP8Z0xWWc86yfbbFe0J2OcRj/Xw7yFuotpiaf5P6ysfDAGdvGPXPV9I9rEpHQ8QErSzR9Zm2VFNhA3GlGn2RPcjYbdY/mhvDxSd1X6AxyEg2xEX6bgZbRNvIFDQsDACbCd7pVTL96f/wrT3i1PhVBKmCKawI3lqPaKvEWpHm4PP8fcmR0lupr7o7CXVhsstERdMaFPp1vS7LoX+wo8HnViFYf/dXXg3Mqq53sxZ0zAvetjw2ELghwVNlDrwZA+gQfeE63QOjzZ67RCFECGMNQCo9Z7 jgrosch@berkeley

EOF


# -------------------------------------------------------------------
#
# Fix ownership
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Fixing the ownership of $ADMIN_USER home directory."
echo "# "
echo "# --------------------------------------------------------------------"
cd /home
chown -R ${ADMIN_USER}:${ADMIN_USER} /home/$ADMIN_USER


# -------------------------------------------------------------------
#
# Create /usr/tmp
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Create /usr/tmp"
echo "# "
echo "# --------------------------------------------------------------------"
if [ ! -e /usr/tmp ];
then
    mkdir /usr/tmp
    chmod 0777 /usr/tmp
fi


# -------------------------------------------------------------------
#
# Is sudo installed
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Is sudo installed? If not install plus facter"
echo "# "
echo "# --------------------------------------------------------------------"
if [ $OS_TYPE == "FreeBSD" ];
then
    echo "FreeBSD sudo"
elif [ $OS_TYPE == "DEBIAN" ];
then
    dpkg -l | grep sudo
    RC=$?
    if [ $RC == 1 ];
    then
        apt update && apt -y install sudo facter
    fi
elif [ $OS_TYPE == "REDHAT" ];
then
    echo "RedHat sudo"
fi


# -------------------------------------------------------------------
#
# add group to sudo
#
# -------------------------------------------------------------------
echo "# --------------------------------------------------------------------"
echo "# "
echo "# Adding the $ADMIN_GROUP group to sudo."
echo "# "
echo "# --------------------------------------------------------------------"
if [ $OS_TYPE == "FreeBSD" ];
then
    pkg info | grep sudo
    RC=$?
    if [ $RC == 1];
    then
        pkg install sudo
    fi
    cd /usr/local/etc/sudoers.d
elif [ $OS_TYPE == "DEBIAN" ];
then
    cd /etc/sudoers.d
elif [ $OS_TYPE == "REDHAT" ];
then
    cd /etc/sudoers.d
fi

echo "%$ADMIN_GROUP ALL=(ALL) NOPASSWD:ALL" > g_admin
chmod 0440 g_admin

exit 0


# -------------------------------------------------------------------
#
#                      < End of AddAdminAccount.sh >
#
# -------------------------------------------------------------------
