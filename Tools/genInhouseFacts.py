#!/usr/bin/env python3

# -----------------------------------------------------------------------
#
#                         < genInhouseFacts.py >
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# File Name    : genInhouseFacts.py
#
# Author       : Josef Grosch
#
# Date         : 05 June 2020
#
# Version      : 1.1
#
# Modification : 1.0   : Basic functionality of this script done (16 Sep 2018)
#                1.1   : Revamp to make this generic
#
# Application  :
#
# Description  : This script pulls the list of installed packages,
#                parses the list looking for inhouse packages and lists
#                them along with their version number in a json formated
#                file, /etc/facter/facts.d/extraInhouse.json. The contents
#                of this file is included by facter.
#
# Notes        :
#
# Functions    :
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                              Copyright
#
#                  (C) Copyright 2020 Moose River, LLC.
#                           All Rights Reserved
#
#                 Deadicated to my brother Jerry Garcia,
#              who passed from this life on August 9, 1995.
#                       Happy trails to you, Jerry
#
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                                GPG Key
#
# pub   rsa4096 2020-04-30 [SC] [expires: 2022-04-30]
#       B696 2527 855D 2098 CAD3  E240 F699 5A87 E863 BA0F
# uid   [ultimate] Josef Grosch <jgrosch@gmail.com>
# sub   rsa4096 2020-04-30 [E] [expires: 2022-04-30]
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                          Contact Information
#
#                           Moose River LLC.
#                             P.O. Box 9403
#                          Berkeley, Ca. 94709
#
#                       http://www.mooseriver.com
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#
# import
#
# -----------------------------------------------------------------------
import os, sys
import json
import subprocess


# -------------------------------------------------------------------
#
# loadConf
#
# -------------------------------------------------------------------
def loadConf(D):
    confFile = D['configFile']

    fp = open(confFile, 'r')
    Lines = fp.read()
    fp.close()
    
    data = json.loads(Lines)

    D['haveInhousePkgs']  = data['haveInhousePkgs']
    D['inhousePkgPrefix'] = data['inhousePkgPrefix']

    return
    #
    # End of loadConf
    #

# -------------------------------------------------------------------
#
# isUbuntu18
#
# -------------------------------------------------------------------
def isUbuntu18(D):
    D['Ubuntu18'] = False
    
    lsbRelease = '/etc/lsb-release'
    
    if os.path.exists(lsbRelease):
        fp = open(lsbRelease, 'r')
        Lines = fp.readlines()
        fp.close()

        for line in Lines:
            if "DISTRIB_RELEASE" in line:
                bits = line.split('=')
                if '18' in bits[1]:
                    D['Ubuntu18'] = True
                else:
                    D['Ubuntu18'] = False
                #
                # End of if
                #
            #
            # End of if
            #
        #
        # End of for loop
        #
    return 
    #
    # End of isUbutu18
    #
    
# -------------------------------------------------------------------
#
# isFreeBSD
#
# -------------------------------------------------------------------
def isFreeBSD(D):
    D['FreeBSD'] = False
    
    cmd = 'uname -o'

    OS = __subprocessCall(cmd)
    
    if 'FreeBSD' in OS:
        D['FreeBSD'] = True
    else:
        D['FreeBSD'] = False

    return 
    #
    # End of isFreeBSD
    #

    
# -------------------------------------------------------------------
#
# getInhouseReleaseStr
#
# -------------------------------------------------------------------
def getInhouseReleaseStr(D):
    cmd = 'cat /etc/{}release'.format(D['inhousePkgPrefix'])

    outStr = __subprocessCall(cmd)
    D['inhouseReleaseStr'] = outStr
    
    return 
    #
    # getInhouseReleaseStr
    #


# -------------------------------------------------------------------
#
# __subprocessCall
#
# -------------------------------------------------------------------
def __subprocessCall(cmd):

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True,  universal_newlines = True)
    (output, err) = p.communicate()
    p_status = p.wait()
    
    return output.strip()
    # End of __subprocessCall

    
# -------------------------------------------------------------------
#
# writePkgJson
#
# -------------------------------------------------------------------
def writePkgJson(D):
    fFile       = D['factsFile']
    fDir        = D['factsDir']
    pkgList     = D['pkgList']
    sitePkgList = D['sitePkgLisr']
    relStr      = D['inhouseReleaseStr']
    prefix      = D['inhousePkgPrefix']

    jFact = open(fFile, 'w')

    outStr = "{\n\"%srelease\": \"%s\",\n" % (prefix, relStr.strip())

    # inhouse packages
    sitePkgCount = len(sitePkgList)
    if sitePkgCount > 0:
        outStr += "\"site_packages\":[\n"
        for key, value in sitePkgList.items():
            aStr = "{\n\"sitePkgName\": \"%s\",\n" % (key)
            bStr = "\"sitePkgVersion\": \"%s\"\n" % (value)
            if sitePkgCount > 1:
                outStr += "%s %s },\n" % (aStr, bStr)
            else:
                outStr += "%s %s }\n" % (aStr, bStr)
            sitePkgCount -= 1
        outStr += "],\n"

    # OS packages
    pkgCount = len(pkgList)
    if pkgCount > 0:
        outStr += "\"system_packages\":[\n"
        for key, value in pkgList.items():
            aStr = "{\n\"pkgName\": \"%s\",\n" % (key)
            bStr = "\"pkgVersion\": \"%s\"\n" % (value)
            if pkgCount > 1:
                outStr += "%s %s },\n" % (aStr, bStr)
            else:
                outStr += "%s %s }\n" % (aStr, bStr)
            pkgCount -= 1
        outStr += "]\n"

    outStr += "}\n"
    jFact.write(outStr)
        
    jFact.close()
    
    return
    #
    # End of writePkgJson
    #

    
# -------------------------------------------------------------------
#
# parseRedHatPkgDump
#
# -------------------------------------------------------------------
def parseRedHatPkgDump(D):
    pkg = {}
    Lines = D['pkgDump']
    Lines = Lines.split('\n')

    pkgFound = False
    
    for line in Lines:
        line = line.strip()

        # Name
        # Version:
        if line.startswith('Name'):
            if 'jnprcfg' in line:
                bits = line.split(':')
                passOne = bits[1].strip()
                passOne = passOne.replace('Relocations', '')
                pkgName = passOne.strip()
                pkgFound = True
            elif 'datagather' in line:
                bits = line.split(':')
                passOne = bits[1].strip()
                passOne = passOne.replace('Relocations', '')
                pkgName = passOne.strip()
                
                #pkgName = bits[1].strip()
                pkgFound = True
            elif 'lynis' in line:
                bits = line.split(':')
                passOne = bits[1].strip()
                passOne = passOne.replace('Relocations', '')
                pkgName = passOne.strip()
                
                #pkgName = bits[1].strip()
                pkgFound = True
            else:
                continue
            
        if pkgFound:
            if line.startswith('Version'):
                bits = line.split(':')
                passOne = bits[1].strip()
                passOne = passOne.replace('Vendor', '')
                pkgVersion = passOne.strip()
                
                #pkgVersion = bits[1].strip()
                pkgFound = False
                pkg[pkgName] = pkgVersion
                continue
        else:
            continue

    D['pkgList'] = pkg
    
    return 
    #
    # End of parseRedHatPkgDump
    #


# -------------------------------------------------------------------
#
# parseFreeBSDPkgDump
#
# -------------------------------------------------------------------
def parseFreeBSDPkgDump(D):
    pkg = {}
    Lines = D['pkgDump']
    Lines = Lines.split('\n')

    for line in Lines:
        line = line.strip()

        strLen = len(line)
        if strLen <= 0:
            continue
        
        bits1 = line.split()
        pkgVer = bits1[0]

        bits2 = pkgVer.split('-')
        #pkgName = bits2[0]
        #pkgVersion = bits2[1]

        if pkgVer.startswith('jnprcfg'):
            pkgName = "{}-{}".format(bits2[0],bits2[1])
            pkgVersion = bits2[2]
            pkg[pkgName] = pkgVersion
        elif pkgVer.startswith('datagather'):
            pkgName = bits2[0]
            pkgVersion = bits2[1]
            pkg[pkgName] = pkgVersion
        elif pkgVer.startswith('lynis'):
            pkgName = bits2[0]
            pkgVersion = bits2[1]
            pkg[pkgName] = pkgVersion

    D['pkgList'] = pkg
        
    return 
    #
    # End of parseFreeBSDPkgDump
    #


# -------------------------------------------------------------------
#
# parseRedHatPkgDump
#
# -------------------------------------------------------------------
def parseDebianPkgDump(D):
    pkg = {}
    Lines = D['pkgDump']
    Lines = Lines.split('\n')

    for line in Lines:
        line = line.strip()

        if not line.startswith('ii'):
            continue

        bits = line.split()
        pkgName = bits[1]
        pkgVersion = bits[2]

        if pkgName.startswith('jnprcfg'):
            pkg[pkgName] = pkgVersion
        elif pkgName.startswith('datagather'):
            pkg[pkgName] = pkgVersion
        elif pkgName.startswith('lynis'):
            pkg[pkgName] = pkgVersion
        else:
            pkg[pkgName] = pkgVersion
            continue

    D['pkgList'] = pkg
    
    return 
    #
    # End of parseDebianPkgDump
    #


# -------------------------------------------------------------------
#
# main starts here
#
# -------------------------------------------------------------------
def main():

    debug = False
    
    configFile = "/usr/local/site/etc/inhousePkgConfig.json"
    factsFile  = "/etc/facter/facts.d/extraInhouse.json"
    factsDir   = "/etc/facter/facts.d"

    if not os.path.exists(factsDir):
        os.makedirs(factsDir, 493)
        
    D = {}
    D['factsFile']  = factsFile
    D['factsDir']   = factsDir
    D['configFile'] = configFile

    loadConf(D)
    
    isFreeBSD(D)
    if D.get('FreeBSD', False):
        bsdFound = True
    else:
        bsdFound = False

    isUbuntu18(D)
    if D.get('Ubuntu18', False):
        u18Found = True
    else:
        u18Found = False
        
    getInhouseReleaseStr(D)

    if bsdFound:
        cmd = 'facter --no-ruby -j'
    else:
        cmd = 'facter -j'

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, universal_newlines = True)
    (output, err) = p.communicate()
    p_status = p.wait()

    if debug:
        print(output)
        
    data = json.loads(output)

    D['kernelVer'] = data['kernelrelease']

    if bsdFound:
        osFamily = data['os']['family']
    else:
        if u18Found:
            osFamily = data['os']['family']
        else:
            osFamily = data['osfamily']

    if osFamily == 'Debian':
        D['osFamily'] = 'Debian'
        cmd = '/usr/bin/dpkg --list'
    elif osFamily == 'RedHat':
        D['osFamily'] = 'RedHat'
        cmd = '/bin/rpm -qai';
    elif osFamily == 'FreeBSD':
        D['osFamily'] = 'FreeBSD'
        cmd = 'pkg info'
    else:
        print("Here be dragons.")
        sys.exit(1)

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True,  universal_newlines = True)
    (output, err) = p.communicate()
    p_status = p.wait()

    D['pkgDump'] = output

    if osFamily == 'Debian':
        parseDebianPkgDump(D)
    elif osFamily == 'RedHat':
        parseRedHatPkgDump(D)
    elif osFamily == 'FreeBSD':
        parseFreeBSDPkgDump(D)

    writePkgJson(D)
    
    sys.exit(0)
    
# -------------------------------------------------------------------
#
# End of main
#
# -------------------------------------------------------------------


# -------------------------------------------------------------------
#
# Entry point
#
# -------------------------------------------------------------------
if __name__ == '__main__':
    main()


# -----------------------------------------------------------------------
#
#                      < End of genInhouseFacts.py >
#
# -----------------------------------------------------------------------


