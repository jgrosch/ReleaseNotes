#!/bin/sh

set -x 

BASE="/usr/local/site"
BIN_PLACE="$BASE/bin"
ETC_PLACE="$BASE/etc"
HOST="repo.mooseriver.com"
PATH="/repo/MiscPkg/Common"

ID=`id -u`
if [ $ID != 0 ]
then
    echo "\n\nERROR: Only root can run this.\n\n"
    exit 1
fi

if [ ! -d $BIN_PLACE ]
then
    mkdir -p $BIN_PLACE
    chmod 0755 $BIN_PLACE
fi 

if [ ! -d $ETC_PLACE ]
then
    mkdir -p $ETC_PLACE
    chmod 0755 $ETC_PLACE
fi 

cd  $ETC_PLACE
wget http://${HOST}:${PATH}/inhouseFactsConfig.json

cd $BIN_PLACE
wget http://${HOST}:${PATH}/genInhouseFacts.py
chown root:root genInhouseFacts.py
chmod 0755 genInhouseFacts.py
$BIN_PLACE/genInhouseFacts.py

exit 0


