# **Release Notes**
# **genReleaseNotes.py**

## Summary

With each release of a JNPR image a release note is required. Writing release notes by hand can be error prone and time consuming. This tool is used to automate the process and will be integrated into the CI/CD process.    

## Prerequisites

The tool, genReleaseNotes.py, has several requirements. Aside from a bog-standard installation of python, the prerequisites are:

  * python 3 -- The tool was written with python3 in mind since this is the where the python project is moving.

  * facter -- This tool needs to be installed in the packer built image for the information it stores.

  * genJuniperFacts.py -- This tool creates extra Juniper facts that are inserted into a facter output.

  * Admin account -- An admin account is needed for genReleaseNotes.py to work. The account must have a ssh key set up and sudo configured to allow the account to gain root privileges without a password. By convention this account is **jnpradmin**.

  **NOTE**: This account only exists for a limited time after first login.

## Setup

The tool, genReleaseNotes.py, resides in the git project **ReleaseNotes**. The data, template, and configuration files reside under the directory **/`<git dir>`/ReleaseNotes**. The tools should be owned by root and have it's permission set to 0755.

Under the directory **/`<git dir>`/ReleaseNotes** there are 5 directories; data, etc, templates, Tools, and WWW that are used by the tool.

  * data

    The release highlights per release are stored here demarked by Operation System family. The directory, Common, contains the html file for a generic highlight.

  * etc

    This directory contains the configuration files for genReleaseNotes.py.

  * templates

    The templates for the various section of the release notes are kept in this directory.

  * Tools

    The Tools directory contains support tools to be used in the handling of the release notes that are gneerated.

  * WWW

    If the tool, genReleaseNotes.py, is invoked with the option --place, the resulting web pages are placed in this directory along with an updated index.html. The tool, pushReleaseNotes.sh, copies the contents of this directory to the web site on repo.juniper.net.



## Configuration

The default configuration file is **`<git dir>`/ReleaseNotes/etc/ReleaseNotes-conf.json**. It is a simple json formated file of key/value pairs with, currently, only three (3) keys; **base-dir**, **data-dir** and **template-dir**. The thinking here is for maximum flexibility the tool should allow the user to place the various components where is suitable.

  * base-dir

    This variable represents the base directory of the tool. All additional directories and files are found under this.


  * data-dir

    This variable maps to the directory is where the data files for each of the releases is located.


  * pkg-list

    This variable points to a json file that lists all the available JNPR packages.


  * template-dir

    The template files for each JNPR package are found in this .


 * url-list


## Environment Variables

  * JNPR_KEY - This environment variable will contain the FQP (Fully Qualified Path) to the private ssh key of the admin account.

## Release Data

## Templates

## Usage

genReleaseNotes support several command line switches. They are:

  * **--blank**

    This option generates a blank release highlight file.


  * **--common**

    This option causes the program to use a generic highlight file.


  * **--config `<config file>`**

    This option selects a custom config file. FQP required.


  * **--debug**

    This toggles the output of debugging information.


  * **--help**

      Outputs this message.


  * **--host `<hostname | IP address>`**

    This is the hostname or the IP address of the newly instantiated instance.


  * **--no_hightlight**

    This tells the program not to include a release highlight section in the release notes.  


  * **--place**

    This option causes the rendered release note to be written to the directory `<git dir>`/ReleaseNotes/WWW/`<OS>`. A side effect is the index.html is re-generated with links to only the latest release notes per Operating System. Without this option the outout of this tool will be written to stdout.


  * **--silence**

    This causes the tool to not output any text.


  * **--testing**

    Turns on testing values.


  * **--version**

    This causes the program to output it's version then exit.


### Example

  * ./genReleaseNotes.py  --host dub.example.com --common

  This will cause a release note to be generated for the system, dub.example.com, using the common AKA generic release hightlight template.


  * ./genReleaseNotes.py --host 192.168.1.1 --no-hightlight

  This will generate a release note for the machine, 192.168.1.1, without a highlight section.

  * ./genReleaseNotes.py --host 192.168.1.12 --common --place

  This will generate a release note for the machine, 192.168.1.12 with the highlight section having a generic entry. The html file will be written to the directory **WWW/`<OS>`**


## Additional Tools

### genJuniperFacts.py

### genJuniperFacts.sh

### pushReleaseNotes.sh
