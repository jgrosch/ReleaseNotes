#!/usr/bin/env python3

# -----------------------------------------------------------------------
#
#                         < genReleaseNotes.py >
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# File Name    : genReleaseNotes.py
#
# Author       : Josef Grosch
#
# Date         : 12 Aug 2018
#
# Version      : 1.9
#
# Modification : 1.1  : Basic functionality of this script done (16 Sep 2018)
#                1.2  : Additional code to support RedHat 6 and CentOS 6
#                1.3  : Added options --blank, --version
#                1.4  : Added code for FreeBSD
#                1.5  : Added --place option and code (04 Apr 2019)
#                1.6  : Added --port option and code (08 May 2019)
#                1.7  : Added --admin-key and --admin-user (10 May 2019)
#                1.8  : Changed --place to a FQP to write to (15 May 2019)
#                1.9  : Added multi quartes on index.html (30 May 2019)
#                1.10 : Convert to OS info json file
#
# Application  :
#
# Description  : This script build the release notes of a given JNPR
#                Image
#
# Notes        :
#
# Functions    :
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                              Copyright
#
#               (C) Copyright 2018 - 2019 Moose River LLC.
#
#                         All Rights Reserved
#
#                 Deadicated to my brother Jerry Garcia,
#              who passed from this life on August 9, 1995.
#                       Happy trails to you, Jerry
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#                                GPG Key
#
# pub rsa4096 2020-04-30 [SC] [expires: 2022-04-30]
# Key fingerprint = B696 2527 855D 2098 CAD3  E240 F699 5A87 E863 BA0F
# uid [ultimate] Josef Grosch <jgrosch@gmail.com>
# sub rsa4096 2020-04-30 [E] [expires: 2022-04-30]
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
#
#                          Contact Information
#
#                           Moose River LLC.
#                             P.O. Box 9403
#                          Berkeley, Ca. 94709
#
#                       http://www.mooseriver.com
#
# -----------------------------------------------------------------------


# -----------------------------------------------------------------------
#
# Import
#
# -----------------------------------------------------------------------
import os, sys, argparse
import datetime
import time
import pprint
import json
import math
import subprocess
from jinja2 import Template, Environment, FileSystemLoader


# -------------------------------------------------------------------
#
# loadOsValue
#
# -------------------------------------------------------------------
def loadOsValues(D):
    osValues = {}
    osInfoFile = "{}/etc/osInfo.json".format(D['cwd'])
    fp = open(osInfoFile, 'r')
    Lines = fp.read()
    fp.close()
    
    data = json.loads(Lines)
    Values = data['osValues']

    for entry in Values:
        osDir          = entry['osDir']
        osName         = entry['osName']
        osRel          = entry['osRel']
        packerSource   = entry['packerSource']
        templateString = entry['templateString']

        key   = osRel

        value = "{\"osDir\": \"%s\", \"osName\": \"%s\", \"osRel\": \"%s\", \"packerSource\": \"%s\", \"templateString\": \"%s\"}" % (osDir, osName, osRel, packerSource, templateString)

        osValues[key] = value

    D['osValues'] = osValues
    
    return
    #
    # End of loadOsValues
    #

# -------------------------------------------------------------------
#
# indexByState
#
# -------------------------------------------------------------------
def indexByState(D):
    osAbbvList    = D['osAbbvList']
    filesNameHash = D['FilesHash']
    outStr        = ""

    for osAbbv in osAbbvList:
        for fileName in filesNameHash:
            if '-' + osAbbv in fileName:
                D['fileName'] = fileName
                outStr += buildEntry(D)
            # End of if
        # End of for
    # End of for

    return outStr
    #
    # End of indexByState
    #

# -------------------------------------------------------------------
#
# buildEntry
#
# -------------------------------------------------------------------
def buildEntry(D):
    osDir    = ""
    osName   = ""
    fileName = D['fileName']
    outStr   = "<li><a target=\"_blank\" "
    bits     = fileName.split('-')

    osRel = bits[2]
    if '.' in osRel:
        bits = osRel.split('.')
        osRel = bits[0]

    if osRel == 'C5':
        osDir  = 'CentOS'
        osName = 'CentOS 5'
    elif osRel == 'C6':
        osDir  = 'CentOS'
        osName = 'CentOS 6'
    elif osRel == 'C7':
        osDir  = 'CentOS'
        osName = 'CentOS 7'
    elif osRel == 'F10':
        osDir  = 'FreeBSD'
        osName = 'FreeBSD 10'
    elif osRel == 'F11':
        osDir  = 'FreeBSD'
        osName = 'FreeBSD 11'
    elif osRel == 'R5':
        osDir  = 'RedHat'
        osName = 'RedHat 5'
    elif osRel == 'R6':
        osDir  = 'RedHat'
        osName = 'RedHat 6'
    elif osRel == 'R7':
        osDir  = 'RedHat'
        osName = 'RedHat 7'
    elif osRel == 'U12':
        osDir  = 'Ubuntu'
        osName = 'Ubuntu 12'
    elif osRel == 'U14':
        osDir  = 'Ubuntu'
        osName = 'Ubuntu 14'
    elif osRel == 'U16':
        osDir  = 'Ubuntu'
        osName = 'Ubuntu 16'
    elif osRel == 'U18':
        osDir  = 'Ubuntu'
        osName = 'Ubuntu 18'

    #href="CentOS/RN-JNPR-C6-2019-03-08-01.html">CentOS 6 (RN-JNPR-C6-2019-03-08-01)
    tmpStr ="href=\"{}/{}\">{} ({})".format(osDir, fileName, osName, fileName)
    outStr += tmpStr
    outStr += "</a></li>&nbsp;\n"

    return outStr
    #
    # End of buildEntry
    #


# -------------------------------------------------------------------
#
# getJulianDatetime
#
# -------------------------------------------------------------------
def getJulianDatetime(year, month, day):
    hour   = 1
    minute = 1
    second = 1
    iYear = int(year)
    iMonth = int(month)
    iDay = int(day)

    partA = int((7 * (iYear + int((iMonth + 9) / 12.0))) / 4.0)
    partB = int((275 * iMonth) / 9.0)
    partC = iDay + 1721013.5
    partD = (hour + minute / 60.0 + second / math.pow(60, 2))
    partE = 24.0 - 0.5 * math.copysign(1, 100 * iYear + iMonth - 190002.5) + 0.5

    jdn = 367 * iYear - partA + partB + partC  + partD / partE

    jdn = int(jdn)

    return jdn
    #
    # End of getJulianDatetime
    #


# -------------------------------------------------------------------
#
# genIndexHead
#
# -------------------------------------------------------------------
def genIndexHead(D):
    today = datetime.date.today()
    year  = today.year
    month = today.month

    if month >= 1 and month <= 3:
        quarter = "first"
    elif month >= 4 and month <= 6:
        quarter = "second"
    elif month >= 7 and month <= 9:
        quarter = "third"
    elif month >= 10 and month <= 12:
        quarter = "fourth"

    outStr = "<!DOCTYPE html>\n<html>\n<head>"
    outStr += "<link rel=\"stylesheet\" href=\"http://repo.juniper.net/styles2.css\">\n"
    outStr += "<title>Current Releases</title>\n</head>\n<body>"
    outStr += "<!-- Begin intro -->\n\n<div class=\"block_text\">\n"
    outStr += "<div class=\"header\"><h1>Release Notes</h1></div>\n"
    outStr += "<table>\n<tbody>\n<tr>\n"
    outStr += "<td width=\"10%\">&nbsp;&nbsp;</td>\n"
    outStr += "<td>The links here point to the release notes for the eight commonly used\n"
    outStr += "Open Source operationing systems used. The release notes\n"
    outStr += "contain links the .ova files that can be downloaded and used\n"
    outStr += "with VMWare.</td>\n<td width=\"10%\">&nbsp;&nbsp;</td>\n"
    outStr += "</tr>\n</tbody>\n</table>\n<p><p><hr><p><p>\n"

    outStr2 = "Hardened images for the {} quarter of {}.\n<p><p>\n".format(quarter, year)

    outStr += outStr2

    return outStr
    #
    # End of genIndexHead
    #

# -------------------------------------------------------------------
#
# GenIndexFoot
#
# -------------------------------------------------------------------
def genIndexFoot(D):
    outStr = "<p><p><hr><p><p>\n</div>\n</body>\n</html>\n"

    return outStr
    #
    # End of genIndexFoot
    #

# -------------------------------------------------------------------
#
# GenIndexList
#
# -------------------------------------------------------------------
def genIndexList(D):
    osAbbvList = ['C5', 'C6', 'C7', 'F10', 'F11', 'R5', 'R6', 'R7', 'U12', 'U14', 'U16', 'U18']
    Files = []
    releasePages = []
    filesNameHash = {}
    emergeNames = {}
    stableNames = {}
    emergeFound = False

    cwd = D['place']
    indexPath = "{}/WWW".format(D['place'])
    os.chdir(indexPath)

    # Load all the files in WWW/*
    for root, dirs, files in os.walk("."):
        for fileName in files:
            if D['debug']:
                print(fileName)
            if ('html' in fileName and 'RN' in fileName):
                Files.append(fileName)

    # walk through the files found, calculate their JDN,
    # and sort them into emerge and stable hashes
    for fileName in Files:
        if 'emerge' in fileName:
            emergeFound = True
        else:
            emergeFound = False

        bits = fileName.split('-')
        name1 = "{}-{}-{}".format(bits[0], bits[1], bits[2])

        if emergeFound:
            fileYear  = bits[5]
            fileMonth = bits[6]
            fileDay   = bits[7]
        else:
            fileYear  = bits[3]
            fileMonth = bits[4]
            fileDay   = bits[5]

        jdn = getJulianDatetime(fileYear, fileMonth, fileDay)
        if emergeFound:
            emergeNames[fileName] = jdn
        else:
            filesNameHash[fileName] = jdn
            stableNames[fileName] = jdn

        emergeFound = False

    if D['debug']:
        pprint.pprint(filesNameHash)

    osFoundFlag = False
    foundFileName = ""

    D['osAbbvList'] = osAbbvList

    outStr = "<p><p><h2>Emerge Images</h2><ul>\n"
    D['FilesHash']  = emergeNames
    outStr += indexByState(D)

    outStr += "</ul><p><p><h2>Stable Images</h2><ul>\n"
    D['FilesHash']  = stableNames
    outStr += indexByState(D)

    outStr += "</ul>\n"

    return outStr
    #
    # End of genIndexList
    #


# -------------------------------------------------------------------
#
# rebuildOsIndex
#
# -------------------------------------------------------------------
def rebuildOsIndex(D):
    outStr = ""
    outStr += genIndexHead(D)
    outStr += genIndexList(D)
    outStr += genIndexFoot(D)

    filePath = "{}/WWW/index.html".format(D['place'])
    fp = open(filePath, "w")
    fp.write(outStr)
    fp.flush()
    os.fsync(fp)
    fp.close()

    return
    #
    # End of rebuildOsINdex
    #

# -------------------------------------------------------------------
#
# isUbuntu18
#
# -------------------------------------------------------------------
def isUbuntu18(D):
    host       = D['host']
    adminKey   = str(D['adminKey'])
    adminUser  = str(D['adminUser'])
    sshPort    = D['sshPort']
    sshOptions = D['sshOptions']
    lsbRelease = '/etc/lsb-release'
    D['Ubuntu18'] = False

    cmd = 'ssh %s -p %s -i %s %s@%s sudo cat %s' % (sshOptions, sshPort, adminKey, adminUser, host, lsbRelease)

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True,  universal_newlines = True)
    (output, err) = p.communicate()
    p_status = p.wait()

    Lines = output.split('\n')

    for line in Lines:
        if "DISTRIB_RELEASE" in line:
            bits = line.split('=')
            if '18' in bits[1]:
                D['Ubuntu18'] = True
            else:
                D['Ubuntu18'] = False
            #
            # End of if
            #
        #
        # End of if
        #
    #
    # End of for loop
    #

    return
    #
    # End of isUbutu18
    #

# -------------------------------------------------------------------
#
# isFreeBSD
#
# -------------------------------------------------------------------
def isFreeBSD(D):
    host       = D['host']
    adminKey   = str(D['adminKey'])
    adminUser  = str(D['adminUser'])
    sshPort    = D['sshPort']
    sshOptions = D['sshOptions']
    debug      = D['debug']

    D['FreeBSD'] = False

    cmd = 'ssh %s -p %s -i %s %s@%s sudo uname -o' % (sshOptions, sshPort, adminKey, adminUser, host)
    if (debug):
        print(cmd)

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, universal_newlines = True)
    (output, err) = p.communicate()
    p_status = p.wait()

    OS = output.strip()
    if 'FreeBSD' in OS:
        D['FreeBSD'] = True
    else:
        D['FreeBSD'] = False

    return
    #
    # End of isFreeBSD
    #


# -----------------------------------------------------------------------
#
# genBlankHighlight
#
# -----------------------------------------------------------------------
def genBlankHighlight():
    filePath = "/tmp/blank-highlight.html"

    outStr = "<!--\nBegin Release Highlights - Common\n-->\n"
    outStr += "\n<ul>\n<li><b>Common</b> - Release Highlights go here !</li>\n"
    outStr += "</ul>\n\n<!-- \nEnd Release Highlights - Common\n-->\n\n"

    fp = open(filePath, "w")
    fp.write(outStr)
    fp.flush()
    os.fsync()
    fp.close()

    #
    # End of genBlankHighlight
    #


# -----------------------------------------------------------------------
#
# getPackagesURL
#
# -----------------------------------------------------------------------
def getPackagesURL(D):
    urlFile = D['url-list']
    urlList = {}

    with open(urlFile, 'r') as fp:
        data = json.load(fp)

    pkgs = data['jnprPackages']

    for key in pkgs:
        pkgName = key['packageName']
        pkgURL  = key['packageGitURL']
        urlList[pkgName] = pkgURL

    D['urlList'] = urlList

    if D['debug']:
        pprint.pprint(urlList)

    return
    #
    # End of getPckagesURL
    #


# -----------------------------------------------------------------------
#
# genReleaseHighlights
#
# -----------------------------------------------------------------------
def genReleaseHighlights(D):
    osName = D['osName']
    release = D['release']
    dataDir = D['data-dir']

    common = D['common']

    if common:
        #JNPR-C7-2018-09-18-01
        #bits = release.split('-')
        #release = "%s-Common-%s-%s-%s-%s" % (bits[0], bits[2], bits[3], bits[4], bits[5])
        filePath = "%s/ReleaseHighlights/Common/Common.html" % (dataDir)
    else:
        filePath = "%s/ReleaseHighlights/%s/RH-%s.html" % (dataDir, osName, release)

    if not os.path.exists(filePath):
        print("\n\nError: %s not found.\n\n" % (filePath))
        sys.exit(1)

    outStr = "<!--\nBegin release-highlights\n-->\n"
    outStr += "<div class=\"block_text\">"
    outStr += "<div class=\"header1\">Release Highlights</div>"

    lines = ""

    with open(filePath, 'r') as fp:
        lines = fp.readlines()

    lines = ''.join(lines)
    outStr += lines

    outStr += "</div>\n<!--\nEnd release-highlights\n-->\n"

    return outStr
    #
    # End of genReleaseHighlights
    #


# -----------------------------------------------------------------------
#
# foremanFormat
#
# -----------------------------------------------------------------------
def foremanFormat(D):
    te = D['TE']
    templateStr = D['templateString']

    bits = templateStr.split('/')

    pkg2 = {"packageName": "foreman-ks"}
    pkg2['preseed'] = templateStr
    pkg2['hostgroup'] = bits[-1:][0]
    pkg2['provisionTemplate'] = bits[-2:-1][0]

    foremanStr = renderTemplate(te, "foreman-ks.j2", pkg2)

    return foremanStr
    #
    # End of foremanFormat
    #


# -----------------------------------------------------------------------
#
# packerFormat
#
# -----------------------------------------------------------------------
def packerFormat(D):
    te = D['TE']
    pkg3 = {
        "packageName": "packer-source",
        "packerSource": D['packerSource']
    }
    packerStr = renderTemplate(te, "packer-source.j2", pkg3)

    return packerStr
    #
    # End of packerFormat
    #


# -----------------------------------------------------------------------
#
# selectTemplateStr
#
# -----------------------------------------------------------------------
def selectTemplateStr(D):

    osName = D['osName']
    osVer  = D['osVersion']

    bits = osVer.split('.')
    osVer = bits[0]

    if osName == 'CentOS':
        if osVer == '6':
            D['templateString'] = D['c6-pt']
            D['packerSource']   = D['c6-ps']
        elif osVer == '7':
            D['templateString'] = D['c7-pt']
            D['packerSource']   = D['c7-ps']
    elif osName == 'RedHatEnterpriseServer':
        if osVer == '7':
            D['templateString'] = D['r7-pt']
            D['packerSource']   = D['r7-ps']
        if osVer == '6':
            D['templateString'] = D['r6-pt']
            D['packerSource']   = D['r6-ps']
    elif osName == 'RedHat':
        if osVer == '7':
            D['templateString'] = D['r7-pt']
            D['packerSource']   = D['r7-ps']
        if osVer == '6':
            D['templateString'] = D['r6-pt']
            D['packerSource']   = D['r6-ps']
    elif osName == 'Ubuntu':
        if osVer == '14':
            D['templateString'] = D['u14-pt']
            D['packerSource']   = D['u14-ps']
        elif osVer == '16':
            D['templateString'] = D['u16-pt']
            D['packerSource']   = D['u16-ps']
        elif osVer == '12':
            D['templateString'] = D['u12-pt']
            D['packerSource']   = D['u12-ps']
        elif osVer == '18':
            D['templateString'] = D['u18-pt']
            D['packerSource']   = D['u18-ps']
    elif osName == 'FreeBSD':
        if osVer == '10':
            D['templateString'] = D['f10-pt']
            D['packerSource']   = D['f10-ps']
        if osVer == '11':
            D['templateString'] = D['f11-pt']
            D['packerSource']   = D['f11-ps']

    return 
    #
    # End of selectTemplateStr
    #


# -----------------------------------------------------------------------
#
# getJuniperFacts
#
# -----------------------------------------------------------------------
def getJuniperFacts(D):

    host       = D['host']
    adminKey   = str(D['adminKey'])
    adminUser  = str(D['adminUser'])
    sshPort    = D['sshPort']
    sshOptions = D['sshOptions']

    if D['FreeBSD']:
        cmd = 'ssh %s -p %s -i %s %s@%s  sudo /usr/local/bin/facter --no-ruby -j' % (sshOptions, sshPort, adminKey, adminUser, host)
    else:
        cmd = 'ssh %s -p %s -i %s %s@%s  sudo /usr/bin/facter -j' % (sshOptions, sshPort, adminKey, adminUser, host)

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True,  universal_newlines = True)
    (output, err) = p.communicate()
    p_status = p.wait()

    data = json.loads(output)

    if 'jnpr_release' in data:
        rel = data['jnpr_release']
        bits = rel.split('-')
        if 'emerge' in rel:
            rel = "%s-%s-%s-%s-%s-%s-%s-%s" % (bits[0], bits[1], bits[2], bits[3], bits[4], bits[5], bits[6], bits[7])
        else:
            rel = "%s-%s-%s-%s-%s-%s-%s" % (bits[0], bits[1], bits[2], bits[3], bits[4], bits[5], bits[6])
        D['release'] = rel
        D['releaseNotesFileName'] = "RN-{}.html".format(rel)


        if D['FreeBSD']:
            a = data['os']['name']
            b = data['os']['release']['full']
            fullRel = "{} {}".format(a, b)
            D['osFamily']   = data['os']['family']
            D['osName']     = data['os']['name']
            D['osVersion']  = data['os']['release']['full']
        elif D['Ubuntu18']:
            fullRel = data['os']['distro']['description']
            D['osFamily']   = data['os']['family']
            D['osName']     = data['os']['name']
            D['osVersion']  = data['os']['distro']['release']['full']
        else:
            fullRel = data['lsbdistdescription']
            D['osFamily']   = data['osfamily']
            D['osName']     = data['lsbdistid']
            D['osVersion']  = data['lsbdistrelease']

        bits = fullRel.split()
        D['fullRel'] = bits[1]
        
        D['jnprPkgs']   = data['jnpr_packages']
        if D['osName'] == 'RedHatEnterpriseServer':
            D['osName'] = 'RedHat'
        D['kernelRel']  = data['kernelrelease']
        D['facterData'] = data

        op = "http://repo.juniper.net/ReleaseNotes/%s/%s.ova" % (D['osName'], rel)
        ovaPath = "<a href=\"%s\">%s</a>" % (op, rel)
        D['ovaPath'] = ovaPath
    else:
        i = 0

    return
    #
    # End of getJuniperFacts
    #


# -----------------------------------------------------------------------
#
# parseForemanInfo
#
# -----------------------------------------------------------------------
def parseForemanInfo(D):
    preseed = D['preseed']
    if 'https' in preseed:
        aStr = preseed.replace('https://','')
    else:
        aStr = preseed.replace('http://','')

    preseed = "<a target=\"_blank\" href=\"{}\">Preseed</a>".format(D['preseed'])
    D['preseed'] = preseed

    bits = aStr.split('/')
    bitCount = len(bits)
    bitCount -= 1

    D['hostgroup'] = bits[bitCount]
    bitCount -= 1
    D['provisionTemplate'] = bits[bitCount]

    return
    #
    # End of parseForemanInfo
    #


# -----------------------------------------------------------------------
#
# todayDate
#
# -----------------------------------------------------------------------
def todayDate():

    p = subprocess.Popen("date", stdout=subprocess.PIPE, shell=True,  universal_newlines = True)

    (output, err) = p.communicate()

    p_status = p.wait()

    return output
    #
    # End of todayDate
    #


# -----------------------------------------------------------------------
#
# insertHR
#
# -----------------------------------------------------------------------
def insertHR():
    return "\n<p><p><hr><p><p>\n"
    #
    #
    #


# -----------------------------------------------------------------------
#
# loadConfig
#
# -----------------------------------------------------------------------
def loadConfig(D):

    configFile = D['configFile']

    if not os.path.exists(configFile):
        print("\n\nERROR: config file, {}, not found\n\n".format(configFile))
        exit(1)

    with open(configFile, 'r') as fp:
        data = json.load(fp)

    for key in data:
        value = data[key]

        D[key] = value
    #
    # End of for loop
    #

    #if D['debug']:
    #    pprint.pprint(D)

    return
    #
    # End of loadConfig
    #


# -----------------------------------------------------------------------
#
# pageHeader
#
# -----------------------------------------------------------------------
def pageHeader(D):
    title = D['release']
    if not title:
        title = "Release Notes"

    outStr = "<!DOCTYPE html>\n<html>\n<head>\n"
    outStr += "<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">\n"
    outStr += "<link rel=\"stylesheet\" href=\"http://repo.juniper.net/sunrise.css\">\n"
    outStr += "<title>%s</title>\n" % (title)
    outStr += "</head>\n<body>\n"

    return outStr
    #
    # End of pageHeader
    #


# -----------------------------------------------------------------------
#
# pageFooter
#
# -----------------------------------------------------------------------
def pageFooter():
    outStr = ""
    outStr += "</body>\n</html>\n"

    return outStr
    #
    # End of pageFooter
    #


# -----------------------------------------------------------------------
#
# parseReleaseName
#
# -----------------------------------------------------------------------
def parseReleaseName(D):
    if D['debug']:
        pprint.pprint(D)

    releaseStr = D['release']
    bits = releaseStr.split(".")

    rel = bits[0]
    date = bits[1]

    bits = rel.split("-")
    os = bits[1]

    osName = os[:1]
    osVer = os[1:]

    D["osVersion"] = osVer

    if osName == 'C':
        D["osName"] = "CentOS"
    elif osName == 'F':
        D["osName"] = "FreeBSD"
    elif osName == 'R':
        D["osName"] = "RedHat"
    elif osName == 'U':
        D["osName"] = "Ubuntu"

    return
    #
    # End of parseReleaseName
    #


# -----------------------------------------------------------------------
#
# renderTemplate
#
# -----------------------------------------------------------------------
def renderTemplate(TE, template_filename, D):
    if 'packageName' in D and 'packageGitURL' in D:
        pkgName = D['packageName']
        pkgURL  = D['packageGitURL']
        aStr = "<a target=\"_blank\" href=\"{}\">{}</a>".format(pkgURL, pkgName)
        D['packageGitURL'] = aStr

    fname = str(template_filename)
    outStr = TE.get_template(fname).render(D)

    return outStr
    #
    # End of renderTemplate
    #


# -----------------------------------------------------------------------
#
# main
#
# -----------------------------------------------------------------------
def main():

    scriptVersion = "1.10"
    dataFileFound = False
    firstTime     = True
    sshPort       = 22
    debug         = False

    #
    # We store data into the hash, D. Why ?
    #
    # 1) Global variables are bad.
    # 2) I hate long parameter lists
    #
    D = {}
    D['cwd'] = os.environ.get('PWD')
    loadOsValues(D)
    defaultConfig = "{}/etc/ReleaseNotes-conf.json".format(D['cwd'])

    #if debug:
    #    pprint.pprint(D)

    adminUser = os.environ.get('ADMIN_USER')
    if adminUser:
        D['adminUser']  = os.environ.get('ADMIN_USER')
        envAdminUserFound = True
    else:
        envAdminUserFound = False

    adminKey = os.environ.get('ADMIN_KEY')
    if adminKey:
        D['adminKey']   = os.environ.get('ADMIN_KEY')
        envAdminKeyFound = True
    else:
        envAdminKeyFound = False

    D['notesDate']  = todayDate()
    D['sshPort']    = sshPort
    D['sshOptions'] = "-o \"StrictHostKeyChecking no\" -o \"UserKnownHostsFile /dev/null\" "

    #
    # Handle the command line parameters
    #
    parser = argparse.ArgumentParser()

    parser.add_argument('--admin-key', help='Admin key to use.',
                        required=False)

    parser.add_argument('--admin-user', help='Admin user.',
                        required=False)

    parser.add_argument('--blank', help='Create a blank release highlight file',
                        action='store_true')

    parser.add_argument('--common', help='Use the common release highlights',
                        action='store_true')

    parser.add_argument('--config', help='FQP to location of the config file',
                        default=defaultConfig, required=False)

    parser.add_argument('--debug', help='Turn on debugging output', action='store_true',
                        required=False)

    parser.add_argument('--host', help='FQ hostname of a running instance.',
                        required=False)

    parser.add_argument('--no-hightlight', help='Include no release highlights',
                        action='store_true')

    parser.add_argument('--place', help='Write the release note in the web site',
                        nargs=1)

    parser.add_argument('--port', help='Use a differt port to ssh on.',
                        default=sshPort)

    parser.add_argument('--silence', help='No output',
                        action='store_true')

    parser.add_argument('--testing', help='Turn on testing', action='store_true',
                        required=False)

    parser.add_argument('--version', help='Spits out the version of this tool',
                        action='store_true', required=False)


    args = parser.parse_args()

    if args.config:
        cfile = str(args.config)
        D['configFile'] = cfile
        loadConfig(D)

    if args.admin_key:
        D['adminKey'] = args.admin_key
    else:
        if envAdminKeyFound:
            D['adminKey'] = adminKey

    if args.admin_user:
        D['adminUser'] = args.admin_user
    else:
        if envAdminUserFound:
            D['adminUser'] = adminUser

    if args.version:
        outStr = "\n\n{} version : {}\n\n".format(os.path.basename(__file__), scriptVersion)
        print(outStr)
        sys.exit(0)

    if args.common:
        D['common'] = True
    else:
        D['common'] = False

    if args.no_hightlight:
        D['no_highlights'] = False
    else:
        D['no_highlights'] = True

    if args.silence:
        D['silence'] = True
    else:
        D['silence'] = False

    if args.blank:
        genBlankHighlight()
        print("\n\nThe blank highlight file has been written to /tmp/blank-highlight.html.\n\n")
        sys.exit(0)

    if args.host == None:
        parser.print_help()
        sys.exit(1)

    if args.port != 22:
        D['sshPort'] = args.port

    if args.place:
        placePath = str(args.place[0])
        if not os.path.exists(placePath):
            os.umask('002')
            os.makedirs(placePath, '0755')
        D['place'] = placePath
    else:
        D['place'] = D['defaultPlace']

    #
    # Code here to check command line args
    #

    D['debug']      = args.debug
    D['testing']    = args.testing
    D['host']       = args.host
    D['blank']      = args.blank

    isFreeBSD(D)
    isUbuntu18(D)

    #
    # The system wide config file,
    # /repo/tools/ReleaseNotes/etc/ReleaseNates-conf.json,
    # gets parsed and the contents are inserted into the
    # data hash
    #
    # loadConfig(D)

    getJuniperFacts(D)
    baseDir = str(D['base-dir'])
    dataDir = str(D['data-dir'])

    getPackagesURL(D)

    TE = Environment(
        autoescape = False,
        loader=FileSystemLoader(os.path.join(baseDir, 'templates')),
        trim_blocks = False
        )

    D['TE'] = TE

    D['date'] = str(datetime.datetime.now())

    selectTemplateStr(D)
    if D['debug']:
        pprint.pprint(D)

    headerStr = pageHeader(D)
    outStr = headerStr

    #
    # Intro
    #
    outStr += renderTemplate(TE, "intro.j2", D)
    outStr += "\n<p><p><hr><p><p>\n"

    #
    # Highligths
    #
    if D['no_highlights']:
        anotherStr = genReleaseHighlights(D)
        outStr += anotherStr
    outStr += "\n<p><p><hr><p><p>\n"

    #
    # Links
    #
    outStr += renderTemplate(TE, "jnpr-links.j2", D)
    outStr += "\n<p><p><hr><p><p>\n"

    outStr2 = ""

    urlList = D['urlList']
    pkgList = D['jnprPkgs']

    if D['debug']:
        pprint.pprint(urlList)

    for pkg in pkgList:
        if firstTime:
            outStr2 += "<div class=\"block_text\">\n"
            outStr2 += "<div class=\"header1\">JNPR Package</div>"
            outStr2 += "<ul>\n"
            firstTime = False

        packageName    = pkg['pkgName']
        packageVersion = pkg['pkgVersion']

        if packageName not in urlList:
            print("\n%s not found in urlList.\n" % (packageName))
            continue

        packageURL     = urlList[packageName]

        outStr3 = "<!--\n Begin %s \n -->\n" % (packageName)
        outStr3 += "<div class=\"header1\">%s</div>\n" % (packageName)
        outStr3 += "<ul>\n<li>Version: %s</li>\n" % (packageVersion)
        outStr3 += "<li>Pkg Git URL: %s</li>\n" % (packageURL)
        outStr3 += "</ul>\n<p><p>\n<!--\nEnd %s\n-->\n" % (packageName)

        outStr2 += "<li>\n"
        outStr2 += outStr3
        outStr2 += "</li>\n"
        #
        # End of for loop
        #

    outStr2 += "</ul>\n</div>\n"

    foremanStr = foremanFormat(D)
    outStr += foremanStr
    outStr += "\n<p><p><hr><p><p>\n"
    packerStr = packerFormat(D)
    outStr += packerStr
    outStr += "\n<p><p><hr><p><p>\n"
    outStr += outStr2
    outStr += "\n<p><p><hr><p><p>\n"

    footerStr = pageFooter()
    outStr += footerStr

    if D['testing']:
        webBase = "/var/www/html/ReleaseNotes/{}".format(D['osName'])
    else:
        webBase = "{}/WWW/{}".format(D['place'], D['osName'])

    if not os.path.exists(webBase):
        os.umask('002')
        os.makedirs(webBase, '0755')

    webPage = "{}/RN-{}.html".format(webBase, D['release'])

    if D['debug']:
        print(outStr)

    if D['place']:
        fp = open(webPage, 'w')
        fp.write(outStr)
        fp.flush()
        os.fsync(fp)
        fp.close()

        rebuildOsIndex(D)
        if D['silence'] == False:
            print("\n\nRelease note written to : %s" % (webPage))
            print("\n\nIndex.html rebuilt. Please push.\n\n")
    else:
        print(outStr)


    sys.exit(0)
    #
    # End of main
    #


# -----------------------------------------------------------------------
#
# Entry point
#
# -----------------------------------------------------------------------
if __name__ == '__main__':
    main()


# -----------------------------------------------------------------------
#
#                      < End of genReleaseNotes.py >
#
# -----------------------------------------------------------------------
